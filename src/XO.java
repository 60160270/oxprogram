import java.util.Scanner;

public class XO {
	static int row , column,turn;
	static String type , input ;
	static String[][] board = new String[3][3];
	
	public static void main(String[] args) {		
		printStart();
		for(turn = 0 ; turn <= 9 ; turn++) {
			printBoard(board);
			if(result(board,turn)) {
				break;
			}
			printTurn(turn);			
			try {
				inputPosition();
				printError(turn);
			}catch(Exception e) {				
				printError();
			}
		}
	}
	
	static void printStart() {
		System.out.println("Start Game OX");	
	}
	static void printBoard(String[][] board) {
		System.out.println("  1 2 3");
		for(int i = 0 ; i < 3 ; i++) {
			System.out.print(i+1+"|");
			for(int j = 0 ; j < 3 ; j++) {
				if(board[i][j]!=null) {
					System.out.print(board[i][j]);	
				}else {
					System.out.print(" ");
				}
				System.out.print("|");				
			}
			System.out.println();
		}
	}
	static boolean result(String[][] board,int turn) {
		String win ;
		for(int i = 0 ; i < 3 ; i++) {
			if(board[i][0]==board[i][1] && board[i][0]==board[i][2] && board[i][i]!=null) {
				win = board[i][0];
				printWin(win);
				return true;
			}
		}
		for(int i = 0 ; i < 3 ; i++) {
			 if(board[0][i]==board[1][i] && board[0][i]==board[2][i] && board[0][i]!=null) {
				win = board[0][i];
				printWin(win);
				return true;
			}
		}
		if(board[0][0]==board[1][1] && board[0][0]==board[2][2] && board[0][0]!=null) {
			win = board[0][0];
			printWin(win);
			return true;
		}else if(board[0][2]==board[1][1] && board[0][2]==board[2][0] && board[0][2]!=null) {
			win = board[0][2];
			printWin(win);
			return true;
		}else if(turn==9) {
			printWin();
			return true;
		}
		return false;
	}
	static void printWin() {
		System.out.println("DRAW");
	}
	static void printWin(String win) {
		System.out.print(win+" : WIN");
	}
	static void inputPosition() {
		Scanner kb = new Scanner(System.in);
		System.out.print("Plz choose position (R,C) :");
		input = kb.next();
		row = Integer.parseInt(input)-1;
		input = kb.next();
		column = Integer.parseInt(input)-1;
	} 
	static void printTurn(int i) {
		if(i%2==0) {
			System.out.println("Turn: X");
			type = "X";
		}else {
			System.out.println("Turn: O");
			type = "O";
		}
	}
	static void printError(){
		turn--;
		System.out.println("Row and Column must be number");
	}
	static void printError(int turn) {
		if(!(row >= 0 && row < 3 && column >= 0 && column < 3)) {
			turn--;
			System.out.println("Row and Column must be number 1 - 3");
		}
		else if(board[row][column]==null) {
			board[row][column] = type ;
			System.out.println();
		}else if(board[row][column]!=null) {
			turn--;
			System.out.println("Row "+row+" and Column "+column+" can't choose again");
		}else {
			turn--;
			System.out.println("Row and Column must be number");
		}
	}
	
}
